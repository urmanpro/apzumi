### **Apzumi**

# Microservice that downloads data at a specific time with several endpoints

### **Starting service in local environment**
cd into project root directory
To setup local environment run
1. Install Mysql database and configure db connection.
2. Create schema 'apzumi' and set it as default 
DB starts on:
localhost:3306/apzumi
user: root
password: root
3. Clone repository with code for example 
git clone https://gitlab.com/shop-ru/auth-common.git
application.properties contains configuration for local environment.
To use it type:
mvn clean package
mvn spring-boot:run

# Instructions for making changes
The most recent changes are always on the master and tagged.
1. Download the latest changes from the master and create a feature branch where we make the changes.
2. Push changes to the feature branch and then create a merge request to the master.
3. Merge changes to the master.
