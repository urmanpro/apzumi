package com.app.apzumichallenge.controller;

import com.app.apzumichallenge.dto.PostDto;
import com.app.apzumichallenge.dto.PostDtoWithoutUser;
import com.app.apzumichallenge.dto.PostUpdateDto;
import com.app.apzumichallenge.service.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/posts")
public class PostController {
    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public ResponseEntity<List<PostDto>> get(){
        return ResponseEntity.ok(postService.getPosts());
    }

    @GetMapping("/find")
    public ResponseEntity<List<PostDtoWithoutUser>> find(@RequestParam(required = false, value = "title") String title){
        return ResponseEntity.ok(postService.find(title));
    }

    @PostMapping()
    public ResponseEntity add(@RequestBody PostDto postDto){
        postService.add(postDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Integer> deletePost(@PathVariable Integer id) {
        postService.deleteById(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<Integer> updatePost(@RequestBody PostUpdateDto postUpdateDto, @PathVariable Integer id) {
        postService.update(postUpdateDto, id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
