package com.app.apzumichallenge.providers;

import com.app.apzumichallenge.dto.PostDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(value = "postClient", url = "${json-service.api.url}")
public interface PostClient {
    @GetMapping(value = "/posts", produces = APPLICATION_JSON_VALUE)
    List<PostDto> get();
}