package com.app.apzumichallenge.exception;

public class IdPostNotFound extends RuntimeException {
    public IdPostNotFound(Integer ID) {
        super("Nie udało sie znależć podanego id  : " + ID + " w bazie danych ");
    }
}