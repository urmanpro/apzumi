package com.app.apzumichallenge.exception;

public class TitleNotFoundException extends RuntimeException {
    public TitleNotFoundException(String title) {
        super("Nie znaleziono tytułu : " + title + " w bazie danych ");
    }
}