package com.app.apzumichallenge.enums;

public enum PostStatusEnum {
    NEW,
    UPDATED,
    DELETED
}
