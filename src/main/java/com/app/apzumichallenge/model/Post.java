package com.app.apzumichallenge.model;
import com.app.apzumichallenge.enums.PostStatusEnum;
import lombok.*;
import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name="user_id")
    private Integer userId;
    @Column(name = "title")
    private String title;
    @Column(name = "body")
    private String body;
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PostStatusEnum status;
}