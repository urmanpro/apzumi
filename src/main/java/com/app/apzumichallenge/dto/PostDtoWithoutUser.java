package com.app.apzumichallenge.dto;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PostDtoWithoutUser {
    private Integer id;
    private String title;
    private String body;
}