package com.app.apzumichallenge.dto;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PostUpdateDto {
    private String title;
    private String body;
}
