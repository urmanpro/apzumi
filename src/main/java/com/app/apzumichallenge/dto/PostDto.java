package com.app.apzumichallenge.dto;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
    private Integer id;
    private Integer userId;
    private String title;
    private String body;
}

