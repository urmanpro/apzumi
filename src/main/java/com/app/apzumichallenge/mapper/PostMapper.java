package com.app.apzumichallenge.mapper;

import com.app.apzumichallenge.dto.PostDtoWithoutUser;
import com.app.apzumichallenge.model.Post;
import org.mapstruct.Mapper;

@Mapper
public interface PostMapper {
    PostDtoWithoutUser map(Post in);
}
