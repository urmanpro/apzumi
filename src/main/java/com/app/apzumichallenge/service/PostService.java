package com.app.apzumichallenge.service;

import com.app.apzumichallenge.dto.PostDto;
import com.app.apzumichallenge.dto.PostDtoWithoutUser;
import com.app.apzumichallenge.dto.PostUpdateDto;
import com.app.apzumichallenge.enums.PostStatusEnum;
import com.app.apzumichallenge.exception.IdPostNotFound;
import com.app.apzumichallenge.exception.TitleNotFoundException;
import com.app.apzumichallenge.mapper.PostMapper;
import com.app.apzumichallenge.model.Post;
import com.app.apzumichallenge.providers.PostClient;
import com.app.apzumichallenge.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PostService {
    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final PostClient postClient;

    public List<PostDtoWithoutUser> find(String title) {
        return title == null ?
                postRepository.findAll().stream().map(this::map).collect(Collectors.toList())
                : findByTitle(title);
    }

    @PostConstruct
    public List<PostDto> getPosts(){
        List<PostDto> postDtos = postClient.get();
        postDtos.forEach(this::add);
        return postDtos;
    }

    private PostDtoWithoutUser map(Post in){
        PostDtoWithoutUser out = postMapper.map(in);
        return out;
    }

    public void add(PostDto postDto) {
            Post post = new Post();
            post.setId(postDto.getId());
            post.setUserId(postDto.getUserId());
            post.setTitle(postDto.getTitle());
            post.setBody(postDto.getBody());
            post.setStatus(PostStatusEnum.NEW);
            postRepository.save(post);
    }

    public void deleteById(Integer postId){
        postRepository.deleteById(postId);
    }

    public void update(PostUpdateDto postUpdateDto, Integer id) {
       Post post = postRepository.findById(id).orElseThrow(() ->
               new IdPostNotFound(id));
       post.setTitle(postUpdateDto.getTitle());
       post.setBody(postUpdateDto.getBody());
       post.setStatus(PostStatusEnum.UPDATED);
       postRepository.save(post);
    }

    public List<PostDtoWithoutUser> findByTitle(String title) {
        List<Post> post = postRepository.findByTitle(title).orElseThrow(() -> new TitleNotFoundException(title));
        return post.stream().map(this::convertToPostWithoutUser).collect(Collectors.toList());
    }

    private PostDtoWithoutUser convertToPostWithoutUser(Post in) {
        return PostDtoWithoutUser.builder()
                .id(in.getId())
                .title(in.getTitle())
                .body(in.getBody())
                .build();
    }
}
