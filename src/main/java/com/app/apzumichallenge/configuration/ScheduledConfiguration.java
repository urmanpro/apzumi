package com.app.apzumichallenge.configuration;

import com.app.apzumichallenge.service.PostService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

@Configuration
@EnableScheduling
public class ScheduledConfiguration {

    private final PostService postService;

    ScheduledConfiguration(PostService postService){
        this.postService = postService;
    }

//    Cron expression is represented by six fields:
//    second, minute, hour, day of month, month, day(s) of week
//    (*) means match any
//    */X means "every X"
    @Scheduled(cron = "0 11 13 * * *" , zone="Europe/Warsaw")
    public void executeTask() {
        System.out.println("Pobieram liste postów z JSON-Placeholdera i zapisuje ją do bazy " + new Date() );
        postService.getPosts();
    }
}