package com.app.apzumichallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ApzumiChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApzumiChallengeApplication.class, args);
	}

}
